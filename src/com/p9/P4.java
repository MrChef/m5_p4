package com.p9;

import java.util.Scanner;

public class P4 {
    private Scanner sc;



    public P4() {
        this.sc = new Scanner(System.in);
    }

    public int readUserInput() {
        return sc.nextInt();
    }

    public void end() {
        System.out.println("FIN");

    }

    public void showMenu() {
        System.out.println("Operacions:");

        System.out.print("Escoge una opción:");
        System.out.println(" ");
        System.out.println("0.Sortir del programa" );
        System.out.println("1.- mulArray() (Angel + Sergi)");
        System.out.println("4.- WordCount (Keneth + Santiago)");

        System.out.println("5.- LetterCount (Berta Álvarez))");
        // Descomenta i crea la teva opció
        System.out.println(" 6.- palindrome() (Daruny)");
        System.out.println("7.- addArray() (JPP + JML)");
        System.out.println("12.- Min_Max_Mitjana (Eric + Jonathan)");
        System.out.println("13.- LetterCount (Cristhian + Yeray)");
        System.out.print("Escoge una opción:");

        System.out.print("Escoge una opción (0 salir):");
    }

    /**
     * Con este metodo el output regresa la palabra al reves.
     */
    public void palindromeWordDASC() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Write a sentence: ");  //INPUT
        String str= sc.nextLine();
        String reverseWord = ""; char ch;
        for (int i=0; i<str.length(); i++){
            ch= str.charAt(i);
            reverseWord= ch + reverseWord;
        }
        System.out.println("Sentence in palindrome way: "+ reverseWord); //OUTPUT

    }


    public void addArray_JML_JPP() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Introduce 5 numeros: ");

        int[] array1 = new int[5];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = sc.nextInt();
        }

        System.out.println("+ + + + +");

        int[] array2 = new int[5];
        for (int i = 0; i < array2.length; i++) {
            array2[i] = sc.nextInt();
        }

        System.out.println("= = = = =");

        int[] arrayFinal = new int[5];
        for (int i = 0; i < arrayFinal.length; i++) {
            arrayFinal[i] = array1[i] + array2[i];
            System.out.print(arrayFinal[i] + " ");
        }


    }

    /**
     * Este programa te muestra cuantas veces aparece una letra que tu quieras en
     * un String que tu escribas.
     */
    public void LetterCount_BAM() {

        Scanner sc=new Scanner(System.in);
        String cadena;
        int contador_letra;
        char letra;


        System.out.println("introduce una palabra o frase");
        cadena=sc.nextLine();
        System.out.println("introduce la letra que quieras buscar");
        letra=sc.next().charAt(0);

        contador_letra=0;

        for(int i=0; i<cadena.length();i++){
            if(cadena.charAt(i)==letra){
                contador_letra++;
            }
        }

        if(contador_letra==0){
            System.out.println("esta frase no contiene la letra "+letra);
        }else{
            System.out.println("la frase contiene la letra "+letra+" "+contador_letra+" veces");
        }
        System.out.println("");

    }


    public void mulArray_API_SAM() {
        int[] array1 = new int[5];
        int[] array2 = new int[5];
        for (int i = 0; i < 5; i++) {
            array1[i] = sc.nextInt();
        }
        for (int i = 0; i < 5; i++) {
            array2[i] = sc.nextInt();
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(array1[i] * array2[i]);
        }
    }


    public void operate(int opcion) {

        switch (opcion) {
            case 0:
                end();
                break;
            case 1:
                mulArray_API_SAM();
                break;
            case 4:
                this.WordCount_KR_LSA();
                break;
            case 5:
                LetterCount_BAM();
                break;
            case 6:
                palindromeWordDASC();
                break;

            case 7:
                addArray_JML_JPP();
                break;
            case 12:
                this.MinMaxMitjana_Eric_Jonathan();
                break;
            case 13:
                this.letterCount_CDZB_YMC();
                break;


        }
    }


    public  void letterCount_CDZB_YMC() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la frase o palabra a calcular su cantidad de letras");
        String str = sc.nextLine();
        int contador = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i)))
                contador++;
        }
        System.out.println("Su frase o palabra tiene esta cantidad de letras: "+contador);

    }

    /**
     * Contador de letras de una cadena.
     */
    public  void WordCount_KR_LSA() {
        System.out.println("Escribe una palabra o frase: ");
        Scanner sc = new Scanner(System.in);

        String cadena = sc.nextLine();
        String str = cadena;
        String cadena2 = cadena;

        String[] palabras = cadena2.split("\\s+");
        String result = str.replaceAll("\\s+", "");
        cadena = cadena.toLowerCase();
        int contador = 0;



        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'b' || cadena.charAt(i) == 'c' || cadena.charAt(i) == 'd' || cadena.charAt(i) == 'e' ||

                    cadena.charAt(i) == 'f' || cadena.charAt(i) == 'g' || cadena.charAt(i) == 'h' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'j' ||

                    cadena.charAt(i) == 'k' || cadena.charAt(i) == 'l' || cadena.charAt(i) == 'm' || cadena.charAt(i) == 'n' || cadena.charAt(i) == 'o' ||

                    cadena.charAt(i) == 'p' || cadena.charAt(i) == 'q' || cadena.charAt(i) == 'r' || cadena.charAt(i) == 's' || cadena.charAt(i) == 't' ||

                    cadena.charAt(i) == 'u' || cadena.charAt(i) == 'v' || cadena.charAt(i) == 'w' || cadena.charAt(i) == 'x' || cadena.charAt(i) == 'y' ||

                    cadena.charAt(i) == 'z') contador++;
        }
        System.out.println("La cadena contiene: " + contador + " caracteres" + " y " + palabras.length + " palabras");
    }

    void MinMaxMitjana_Eric_Jonathan(){
        System.out.println("Introduce valores, para finalizar este proceso escribe 0");
        int numero = sc.nextInt();
        int numeromenor = numero;
        int nummayor = 0;
        int contador = 0;
        int numerototal = 0;


        while (numero != 0) {
            if (numero > nummayor) {
                nummayor = numero;
            } else if (numero <= numeromenor) {
                numeromenor = numero;
            }
            numero = sc.nextInt();
            contador = contador + 1;
            numerototal = numerototal + numero;
        }
        System.out.println("El valor mas bajo ha sido: " + numeromenor);
        System.out.println("El valor mas alto ha sido: " + nummayor);
        System.out.println("La media de todos los numeros introducidos a sido: " + numerototal / contador);



    }

    public static void main(String[] args) {

        P4 p4 = new P4();

        int response;
        do {
            p4.showMenu();
            response = p4.readUserInput();
            p4.operate(response);
        } while (response != 0);

    }


}
